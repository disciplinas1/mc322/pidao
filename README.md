# Pidão

Food delivery app system.

## Project Decisions:

Execution flow is exemplified in Runner and detailed below.

### Execution flow

- Creating a new pidao app.

- Using a Pidao instance to register new restaurants, users and deliverymen.

- Associating deliverymen to restaurants by hire and fire methods.

- Creating food instances and adding them to a restaurant's menu.

- Creating orders, adding items to them and starting them.

- Printing a restaurant's menu.

- Print a summary of all active orders.
