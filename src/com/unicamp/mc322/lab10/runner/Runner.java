package com.unicamp.mc322.lab10.runner;

import com.unicamp.mc322.lab10.app.*;

public class Runner {

	public static void main(String[] args) {

		Pidao pidao = new Pidao();

		Restaurant dominos = pidao.registerRestaurant("Domino's", "123.142.584-12", 8, 5);
		Restaurant pizzaHut = pidao.registerRestaurant("Pizza Hut", "345.178.190-65", 1, 2);
		Restaurant lastro = pidao.registerRestaurant("Lastro", "128.234.823-81", 2, 3);

		User user1 = pidao.registerUser("Zezé", "237.367.124-12", 5, 3);
		User user2 = pidao.registerUser("Ma Pereira", "325.789.643-11", 8, 4);

		Deliveryman deliveryman1 = pidao.registerDeliveryman("Larissa Campos", "127.453.128-10");
		Deliveryman deliveryman2 = pidao.registerDeliveryman("Jota Silva", "234.778.269-13");
		dominos.hireDeliveryman(deliveryman1);
		dominos.hireDeliveryman(deliveryman2);
		pizzaHut.hireDeliveryman(deliveryman1);
		lastro.hireDeliveryman(deliveryman2);


		Food mussarela = new Food("AA000", "Pizza de Mussarela", 24.00);
		Food peperoni = new Food("AA021", "Pizza de Peperoni", 31.00);
		Food lombo = new Food("AA033", "Pizza de Lombo", 30.00);
		Food bacon = new Food("AA034", "Pizza de Bacon", 28.00);
		Food calzone = new Food("AB003", "Calzone", 34.00);
		Food brigadeiro = new Food("AC001", "Pizza de Brigadeiro", 20.00);
		Food banana = new Food("AC005", "Pizza de Banana", 20.00);

		dominos.addToMenu(mussarela);
		dominos.addToMenu(peperoni);

		pizzaHut.addToMenu(lombo);
		pizzaHut.addToMenu(calzone);
		pizzaHut.addToMenu(banana);

		lastro.addToMenu(bacon);
		lastro.addToMenu(brigadeiro);

		dominos.applyDiscount("AA000", 15, DiscountType.PERCENTAGE);
		dominos.applyDiscount("AA021", 5, DiscountType.VALUE);
		lastro.applyDiscount("AC001", 20, DiscountType.PERCENTAGE);

		Order o1 = dominos.createOrder(user1);
		o1.addFood(mussarela);
		o1.addFood(mussarela);
		dominos.startOrder(o1, OrderType.DELIVERY);
		dominos.finishPreparing(o1);
		dominos.startDelivery(o1);
		dominos.finishDelivery(o1);
		o1.rateDeliveryman(5, "Fast delivery!");
		o1.rateFoods(4, "");
		o1.rateRestaurant(4, "My preffered pizza in the city!");

		Order o2 = pizzaHut.createOrder(user2);
		o2.addFood(lombo);
		o2.addFood(calzone);
		o2.addFood(banana);
		pizzaHut.startOrder(o2, OrderType.PICKUP);
		pizzaHut.finishPreparing(o2);
		pizzaHut.finishPickUp(o2);
		o2.rateRestaurant(5, "Perfect pizza!");

		Order o3 = lastro.createOrder(user2);
		o3.addFood(bacon);
		o3.addFood(bacon);
		o3.addFood(brigadeiro);
		lastro.startOrder(o3, OrderType.DELIVERY);
		lastro.finishPreparing(o3);
		lastro.startDelivery(o3);
		lastro.finishDelivery(o3);
		o3.rateDeliveryman(5, "");
		o3.rateRestaurant(4, "Very nice but a little expensive.");

		pizzaHut.printOrdersSummary();
		dominos.printMenu();
		pidao.printRestaurantsRatings(true);
		pidao.printDeliverymanRatings();
	}
}
