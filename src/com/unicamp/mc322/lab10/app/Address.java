package com.unicamp.mc322.lab10.app;

public class Address {

    // Properties
    private int xCoordinate;
    private int yCoordinate;

    // Constructor
    Address(int xCoordinate, int yCoordinate) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
    }

    // Methods
    public void showInformation() {
        this.showInformation("");
    }

    public void showInformation(String indentation) {
        System.out.println(indentation + this);
    }

    public double getDistanceTo(Address other) {
        int deltaX = this.xCoordinate - other.xCoordinate;
        int deltaY = this.yCoordinate - other.yCoordinate;
        return Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
    }

    @Override
    public String toString() {
        return "Address: (" + xCoordinate + ", " + yCoordinate + ")";
    }
}
