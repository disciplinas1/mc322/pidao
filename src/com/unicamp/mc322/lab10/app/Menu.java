package com.unicamp.mc322.lab10.app;

import java.util.TreeSet;

public class Menu {

    // Properties
    private TreeSet<Food> foods;

    // Constructor
    Menu() {
        this.foods = new TreeSet<Food>();
    }

    // Methods
    void print() {
        this.print("");
    }

    void print(String indentation) {
        for (Food food: this.foods) {
            food.showInformation(indentation);
        }
    }

    void printAllFoodRatings() {
        this.printAllFoodRatings("");
    }

    void printAllFoodRatings(String indentation) {
        for (Food food: this.foods) {
            food.printRatings(indentation);
        }
    }

    void removeDiscount(String identifier) {
        Food food = this.getFood(identifier);
        if (food == null) {
            System.out.println("Trying to remove discount of non-existing food: " + identifier);
            return;
        }

        food.removeDiscount();
    }

    void applyDiscount(String identifier, double value, DiscountType discountType) {
        Food food = this.getFood(identifier);
        if (food == null) {
            throw new IllegalArgumentException("Trying to add discount to non-existing food: " + identifier);
        }

        food.applyDiscount(value, discountType);
    }

    void removeFood(String identifier) {
        Food food = this.getFood(identifier);
        if (food == null) {
            System.out.println("Trying to remove non-existing food: " + identifier);
            return;
        }

        this.foods.remove(food);
    }

    void addFood(Food food) {
        String identifier = food.getIdentifier();
        if (this.getFood(identifier) != null) {
            throw new IllegalArgumentException("Repeated identifier: " + identifier);
        }

        this.foods.add(food);
    }

    private Food getFood(String identifier) {
        for (Food food: this.foods) {
            if (food.hasIdentifier(identifier)) {
                return food;
            }
        }

        return null;
    }
}
