package com.unicamp.mc322.lab10.app;

public class Discount {

    // Properties
    private double value;
    private DiscountType discountType;

    // Constructor
    Discount(double value, DiscountType discountType) {
        switch (discountType) {
            case PERCENTAGE:
                this.discountType = DiscountType.PERCENTAGE;
                this.defineValueForPercentageType(value);
                break;
            case VALUE:
                this.discountType = DiscountType.VALUE;
                this.defineValueForValueType(value);
                break;
        }
    }

    // Methods
    double applyDiscount(double amount) {
        double finalValue = 0.0;
        switch (this.discountType) {
            case PERCENTAGE:
                finalValue = amount * (100 - this.value) / 100;
                break;
            case VALUE:
                finalValue = Math.max(0.0, amount - value);
                break;
            default:
                finalValue = amount;
                break;
        }

        return finalValue;
    }

    private void defineValueForPercentageType(double value) {
        if (value < 0 || value > 100) {
            throw new IllegalArgumentException("Discount in percentage with invalid value: " + value);
        }

        this.value = value;
    }

    private void defineValueForValueType(double value) {
        if (value < 0) {
            throw new IllegalArgumentException("Invalid value discount: " + value);
        }

        this.value = value;
    }
}
