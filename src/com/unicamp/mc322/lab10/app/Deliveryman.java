package com.unicamp.mc322.lab10.app;

import java.util.ArrayList;

public class Deliveryman extends Person implements Rateable {

    // Properties
    private ArrayList<Order> orders;
    private ArrayList<Rating> ratings;

    // Constructor
    Deliveryman(String name, String cpf) {
        super(name, cpf);
        this.orders = new ArrayList<Order>();
        this.ratings = new ArrayList<Rating>();
    }

    // Methods
    void addOrder(Order newOrder) {
        this.orders.add(newOrder);
    }

    @Override
    public ArrayList<Rating> getRatings() {
        return this.ratings;
    }
}
