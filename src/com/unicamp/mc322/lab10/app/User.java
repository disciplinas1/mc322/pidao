package com.unicamp.mc322.lab10.app;

import java.util.ArrayList;

public class User extends Person {

    // Properties
    private Address address;
    private ArrayList<Order> orders;

    // Constructor
    User(String name, String cpf, Address address) {
        super(name, cpf);
        this.address = address;
        this.orders = new ArrayList<Order>();
    }

    // Methods
    @Override
    void showInformation() {
        super.showInformation();
        System.out.println("Address: " + this.address);
    }

    void addOrder(Order newOrder) {
        this.orders.add(newOrder);
    }

    int getOrderPositionOnList(Order order) {
        return this.orders.indexOf(order);
    }

    Address getAddress() {
        return this.address;
    }

}
