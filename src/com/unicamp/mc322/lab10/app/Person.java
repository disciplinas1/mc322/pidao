package com.unicamp.mc322.lab10.app;

public abstract class Person implements Comparable<Person> {

    // Properties
    private String name;
    private String cpf;

    // Constructor
    Person(String name, String cpf) {
        this.name = name;
        this.cpf = cpf;
    }

    // Methods
    void showInformation() {
        System.out.println("User name: " + this.name);
        System.out.println("CPF: " + this.cpf);
    }

    String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return this.name + " (" + this.cpf + ")";
    }

    @Override
    public int compareTo(Person other) {
        return this.name.compareTo(other.name);
    }
}
