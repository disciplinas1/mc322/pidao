package com.unicamp.mc322.lab10.app;

public enum OrderStatus {
    NEW("Not confirmed"),
    PREPARING("Preparing"),
    PREPARED("Prepared"),
    DELIVERING("Delivering"),
    FINISHED("Finished"),
    CANCELED("Canceled");

    // Properties
    private String label;

    // Constructor
    private OrderStatus(String label) {
        this.label = label;
    }

    // Methods
    String getLabel() {
        return this.label;
    }

}
