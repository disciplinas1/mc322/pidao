package com.unicamp.mc322.lab10.app;

import java.util.ArrayList;

public class Food implements Comparable<Food>, Rateable {

    // Properties
    private String identifier;
    private String name;
    private double price;
    private Discount discount;
    private ArrayList<Rating> ratings;

    // Constructor
    public Food(String identifier, String name, double price) {
        if (!Food.checkIdentifier(identifier)) {
            throw new IllegalArgumentException("Invalid identifier: " + identifier);
        }

        this.identifier = identifier.toUpperCase();
        this.name = name;
        this.price = price;
        this.discount = null;
        this.ratings = new ArrayList<Rating>();
    }

    // Methods
    public void showInformation() {
        this.showInformation("");
    }

    void showInformation(String indentation) {
        System.out.println(indentation + this);
    }

    @Override
    public String toString() {
        return "[" + this.identifier + "] " + this.name + " " + this.getPriceAsString() + this.getDiscountAsString();
    }

    void applyDiscount(double value, DiscountType discountType) {
        this.discount = new Discount(value, discountType);
    }

    void removeDiscount() {
        this.discount = null;
    }

    boolean hasIdentifier(String identifier) {
        return this.identifier == identifier.toUpperCase();
    }

    @Override
    public int compareTo(Food other) {
        return this.identifier.compareTo(other.identifier);
    }

    String getIdentifier() {
        return this.identifier;
    }

    private String getPriceAsString() {
        double amount = this.getPrice();
        return String.format("R$%.2f", amount);
    }

    private String getDiscountAsString() {
        String discountAsString = "";
        if (this.discount != null) {
            discountAsString = String.format(" (SALE! Regular price: R$%.2f)", this.price);
        }

        return discountAsString;
    }

    double getPrice() {
        double amount = 0.0;
        if (this.discount == null) {
            amount = this.price;
        } else {
            amount = this.discount.applyDiscount(this.price);
        }

        return amount;
    }

    @Override
    public ArrayList<Rating> getRatings() {
        return this.ratings;
    }

    // Static Methods
    private static boolean checkIdentifier(String identifier) {
        return identifier.length() == 5;
    }
}
