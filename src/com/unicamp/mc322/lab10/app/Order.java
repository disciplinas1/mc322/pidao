package com.unicamp.mc322.lab10.app;

import java.util.ArrayList;
import java.util.Optional;
import java.util.TreeSet;

public class Order {

    // Static Properties
    private static double deliveryPricePerDistance = 0.5;
    private static double firstOrderPercentualDiscount = 20;

    // Properties
    private User user;
    private Restaurant restaurant;
    private ArrayList<Food> foods;
    private OrderStatus status;
    private OrderType type;
    private Optional<Deliveryman> deliveryman;

    // Constructor
    public Order(User user, Restaurant restaurant) {
        this.user = user;
        this.restaurant = restaurant;
        this.foods = new ArrayList<Food>();
        this.status = OrderStatus.NEW;
        this.type = OrderType.UNDEFINED;
        this.deliveryman = Optional.empty();

        this.user.addOrder(this);
    }

    // Methods
    void showInformation() {
        this.showInformation("");
    }

    void showInformation(String indentation) {
        System.out.println(indentation + "User: " + this.user);
        for (Food food: this.foods) {
            System.out.println(indentation + "- " + food.getIdentifier());
        }

        System.out.println(indentation + String.format("Total value: R$%.2f", this.getTotalValue()));
        System.out.println(indentation + "Order status: " + this.status.getLabel());
        System.out.println(indentation + "Delivery type: " + this.type.getLabel());
    }

    private double getTotalValue() {
        double value = 0.0;
        for (Food food: this.foods) {
            value += food.getPrice();
        }

        switch (this.type) {
            case UNDEFINED:
                System.out.println("Trying to get price of order with undefined delivery type.");
                break;
            case DELIVERY:
                value += this.getDeliveryPrice();
                break;
            default:
                break;
        }

        return this.applyOrderDiscount(value);
    }

    private double getDeliveryPrice() {
        Address restaurantAddress = this.restaurant.getAddress();
        Address userAddress = this.user.getAddress();
        double distance = restaurantAddress.getDistanceTo(userAddress);
        return deliveryPricePerDistance * distance;
    }

    private double applyOrderDiscount(double value) {
        int orderNumber = this.user.getOrderPositionOnList(this);
        double finalValue;

        if (orderNumber == 0) {
            finalValue = applyFirstOrderDiscount(value);
        } else {
            finalValue = value;
        }

        return finalValue;
    }

    private double applyFirstOrderDiscount(double value) {
        Discount discount = new Discount(firstOrderPercentualDiscount, DiscountType.PERCENTAGE);
        return discount.applyDiscount(value);
    }

    public void addFood(Food food) {
        this.foods.add(food);
    }

    public void rateRestaurant(int stars, String comment) {
        this.restaurant.rate(this, this.user, stars, comment);
    }

    public void rateDeliveryman(int stars, String comment) {
        this.deliveryman.ifPresentOrElse(
            deliveryman -> {
                deliveryman.rate(this, this.user, stars, comment);
            },
            () -> {
                System.out.println("Trying to rate deliveryman in order with no deliveryman assigned.");
            }
        );
    }

    public void rateFoods(int stars, String comment) {
        TreeSet<Food> uniqueFoods = new TreeSet<Food>(this.foods);
        for (Food food: uniqueFoods) {
            food.rate(this, this.user, stars, comment);
        }
    }

    void startToPrepare(OrderType type) {
        if (type == OrderType.UNDEFINED) {
            System.out.println("Trying to start order with undefined delivery type.");
            return;
        }

        this.type = type;

        switch (this.status) {
            case NEW:
                this.status = OrderStatus.PREPARING;
                break;
            default:
                System.out.println("Order already confirmed.");
                break;
        }
    }

    void finishPreparing() {
        switch (this.status) {
            case NEW:
                System.out.println("Order not confirmed yet.");
                break;
            case PREPARING:
                this.status = OrderStatus.PREPARED;
                break;
            default:
                System.out.println("Order has already left for delivery.");
                break;
        }
    }

    void startDelivery(Deliveryman deliveryman) {
        if (this.type != OrderType.DELIVERY) {
            System.out.println("Trying to delivery order with wrong type: " + this.type.getLabel());
            return;
        }

        this.deliveryman = Optional.of(deliveryman);

        switch (this.status) {
            case NEW:
            case PREPARING:
                System.out.println("Order not prepared yet.");
                break;
            case PREPARED:
                this.status = OrderStatus.DELIVERING;
                break;
            default:
                System.out.println("Order already left for delivery.");
                break;
        }
    }

    void finishPickUp() {
        if (this.type != OrderType.PICKUP) {
            System.out.println("Trying to pick up order with wrong type: " + this.type.getLabel());
            return;
        }

        switch (this.status) {
            case FINISHED:
                System.out.println("Order already picked up.");
                break;
            case PREPARED:
                this.status = OrderStatus.FINISHED;
                break;
            default:
                System.out.println("Order not prepared yet.");
                break;
        }
    }

    void finishDelivery() {
        if (this.type != OrderType.DELIVERY) {
            System.out.println("Trying to delivery order with wrong type: " + this.type.getLabel());
            return;
        }

        switch (this.status) {
            case FINISHED:
                System.out.println("Order already delivered.");
                break;
            case DELIVERING:
                this.status = OrderStatus.FINISHED;
                break;
            default:
                System.out.println("Order did not leave for delivery yet.");
                break;
        }
    }

    boolean cancel() {
        switch (this.status) {
            case NEW:
            case PREPARING:
                this.status = OrderStatus.CANCELED;
                return true;
            case PREPARED:
            case DELIVERING:
            case FINISHED:
                System.out.println("Order is already prepared and cannot be canceled.");
                return false;
            default:
                return false;
        }
    }

    boolean isDelivery() {
        return this.type == OrderType.DELIVERY;
    }
}
