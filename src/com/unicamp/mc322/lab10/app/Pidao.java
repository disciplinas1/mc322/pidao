package com.unicamp.mc322.lab10.app;

import java.util.TreeSet;

public class Pidao {

    // Properties
    private TreeSet<Restaurant> restaurants;
    private TreeSet<User> users;
    private TreeSet<Deliveryman> deliverymen;

    // Constructor
    public Pidao() {
        this.restaurants = new TreeSet<Restaurant>();
        this.users = new TreeSet<User>();
        this.deliverymen = new TreeSet<Deliveryman>();
    }

    // Methods
    public Restaurant registerRestaurant(String name, String cnpj, int xCoordinate, int yCoordinate) {
        Restaurant restaurant = new Restaurant(name, cnpj, new Address(xCoordinate, yCoordinate));
        this.restaurants.add(restaurant);
        return restaurant;
    }

    public User registerUser(String name, String cpf, int xCoordinate, int yCoordinate) {
        User user = new User(name, cpf, new Address(xCoordinate, yCoordinate));
        this.users.add(user);
        return user;
    }

    public Deliveryman registerDeliveryman(String name, String cpf) {
        Deliveryman deliveryman = new Deliveryman(name, cpf);
        this.deliverymen.add(deliveryman);
        return deliveryman;
    }

    public void printRestaurantsRatings(boolean showFoods) {
        for (Restaurant restaurant: this.restaurants) {
            if (showFoods) {
                restaurant.printRatingsWithFoods();
            } else {
                restaurant.printRatings();
            }
        }
    }

    public void printDeliverymanRatings() {
        for (Deliveryman deliveryman: this.deliverymen) {
            deliveryman.printRatings();
        }
    }

}
