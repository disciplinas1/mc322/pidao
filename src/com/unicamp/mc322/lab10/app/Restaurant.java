package com.unicamp.mc322.lab10.app;

import java.util.ArrayList;

public class Restaurant implements Comparable<Restaurant>, Rateable {

    // Properties
    private String name;
    private String cnpj;
    private Address address;
    private Menu menu;
    private ArrayList<Order> orders;
    private ArrayList<Deliveryman> deliverymen;
    private ArrayList<Rating> ratings;

    // Constructor
    Restaurant(String name, String cnpj, Address address) {
        this.name = name;
        this.cnpj = cnpj;
        this.address = address;
        this.menu = new Menu();
        this.orders = new ArrayList<Order>();
        this.deliverymen = new ArrayList<Deliveryman>();
        this.ratings = new ArrayList<Rating>();
    }

    // Methods
    public void printMenu() {
        System.out.println("Restaurant: " + this.name);
        System.out.println("(CNPJ: " + this.cnpj + ")");
        this.address.showInformation();
        System.out.println();
        System.out.println("Today's menu:");
        this.menu.print();
        System.out.println();
    }

    public void printOrdersSummary() {
        String sep = "===================================";
        System.out.println("Restaurant: " + this.name);
        System.out.println("There are " + this.orders.size() + " orders:");
        System.out.println(sep);
        for (Order order: this.orders) {
            order.showInformation();
            System.out.println(sep);
        }
    }

    public void printRatingsWithFoods() {
        this.printRatingsWithFoods("");
    }

    void printRatingsWithFoods(String indentation) {
        this.printRatings(indentation);
        this.menu.printAllFoodRatings(indentation + "    ");
    }

    public void hireDeliveryman(Deliveryman deliveryman) {
        this.deliverymen.add(deliveryman);
    }

    public void fireDeliveryman(Deliveryman deliveryman) {
        this.deliverymen.remove(deliveryman);
    }

    public Order createOrder(User user) {
        Order order = new Order(user, this);
        this.orders.add(order);
        return order;
    }

    public void startOrder(Order order, OrderType type) {
        order.startToPrepare(type);
    }

    public void finishPreparing(Order order) {
        if (!this.orders.contains(order)) {
            throw new IllegalArgumentException("Non-started order cannot be prepared.");
        }

        order.finishPreparing();
    }

    public void startDelivery(Order order) {
        if (!this.orders.contains(order)) {
            throw new IllegalArgumentException("Non-started order cannot start delivery.");
        }

        if (this.deliverymen.size() < 1) {
            System.out.println("No deliverymen available for order.");
            return;
        }

        Deliveryman deliveryman = this.deliverymen.remove(0);
        this.deliverymen.add(deliveryman);
        deliveryman.addOrder(order);
        order.startDelivery(deliveryman);
    }

    public void finishPickUp(Order order) {
        if (!this.orders.contains(order)) {
            throw new IllegalArgumentException("Non-started order cannot be finished.");
        }

        order.finishPickUp();
    }

    public void finishDelivery(Order order) {
        if (!this.orders.contains(order)) {
            throw new IllegalArgumentException("Non-started order cannot be finished.");
        }

        order.finishDelivery();
    }

    public void removeFromMenu(String identifier) {
        this.menu.removeFood(identifier);
    }

    public void addToMenu(Food food) {
        this.menu.addFood(food);
    }

    public void removeDiscount(String identifier) {
        this.menu.removeDiscount(identifier);
    }

    public void applyDiscount(String identifier, double value, DiscountType discountType) {
        this.menu.applyDiscount(identifier, value, discountType);
    }

    public String getName() {
        return this.name;
    }

    Address getAddress() {
        return this.address;
    }

    @Override
    public String toString() {
        return this.name + " (" + this.cnpj + ")";
    }

    @Override
    public int compareTo(Restaurant other) {
        return this.name.compareTo(other.name);
    }

    @Override
    public ArrayList<Rating> getRatings() {
        return this.ratings;
    }
}
