package com.unicamp.mc322.lab10.app;

import java.util.ArrayList;

public interface Rateable {

    // Methods
    ArrayList<Rating> getRatings();

    // Default Implementations
    default void rate(Order order, User user, int stars, String comment) {
        if (this.hasRatingFromOrder(order)) {
            System.out.println("Trying to rate item twice from same order.");
        }

        this.getRatings().add(new Rating(order, user, stars, comment));
    }

    default boolean hasRatingFromOrder(Order order) {
        for (Rating rating: this.getRatings()) {
            if (rating.getOrder() == order) {
                return true;
            }
        }

        return false;
    }

    default void printRatings() {
        this.printRatings("");
    }

    default void printRatings(String indentation) {
        ArrayList<Rating> ratings = this.getRatings();
        double average = this.getAverageRating();


        System.out.println(indentation + "Ratings for " + this);
        System.out.println(indentation + "Average rating: " + String.format("%.2f", average));
        for (Rating rating: ratings) {
            rating.print(indentation + "    ");
        }
    }

    default double getAverageRating() {
        ArrayList<Rating> ratings = this.getRatings();
        if (ratings.isEmpty()) {
            return 0;
        }

        double sum = 0;
        for (Rating rating: ratings) {
            sum += rating.getStars();
        }

        return sum / ratings.size();
    }

}
