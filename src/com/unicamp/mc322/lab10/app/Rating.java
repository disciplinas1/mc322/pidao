package com.unicamp.mc322.lab10.app;

public class Rating {

    // Static Properties
    private static int maxStars = 5;

    // Properties
    private Order order;
    private User user;
    private int stars;
    private String comment;

    // Constructor
    Rating(Order order, User user, int stars, String comment) {
        if (stars < 0 || stars > maxStars) {
            throw new IllegalArgumentException("Invalid number of stars for rating: " + stars);
        }

        this.order = order;
        this.user = user;
        this.stars = stars;
        this.comment = comment;
    }

    // Methods
    public void print(String indentation) {
        System.out.println(indentation + this.user.getName() + " " + this.getStarsAsString());
        if (!this.comment.isEmpty()) {
            System.out.println(indentation + "Comment: " + this.comment);
        }
    }

    private String getStarsAsString() {
        String stars = "";

        for (int i = 0; i < maxStars; i++) {
            stars += i < this.stars ? "*" : " ";
        }

        return "(" + stars + ")";
    }

    int getStars() {
        return this.stars;
    }

    Order getOrder() {
        return this.order;
    }
}
