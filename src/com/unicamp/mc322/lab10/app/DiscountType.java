package com.unicamp.mc322.lab10.app;

public enum DiscountType {
    PERCENTAGE,
    VALUE;
}
