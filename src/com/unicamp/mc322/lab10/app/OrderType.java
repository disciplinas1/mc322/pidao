package com.unicamp.mc322.lab10.app;

public enum OrderType {
    UNDEFINED("Undefined"),
    DELIVERY("Delivery"),
    PICKUP("Pick up");

    // Properties
    private String label;

    // Constructor
    private OrderType(String label) {
        this.label = label;
    }

    // Methods
    String getLabel() {
        return this.label;
    }

}
